package com.maksimLarchanka.TestTask4;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskFour {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String words = scanner.nextLine();
        int count = 0;

        String sortWords = words.replaceAll("\\p{Punct}", "");

        Pattern p = Pattern.compile("[a-zA-Zа-яА-Я]+");
        Matcher m = p.matcher(words);

        while (m.find()) {
            count++;
        }
        System.out.println("sorted words : " + sortWords);
        System.out.println("number of words : " + count);
    }
}