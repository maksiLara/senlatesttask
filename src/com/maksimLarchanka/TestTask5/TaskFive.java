package com.maksimLarchanka.TestTask5;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String allWords = scanner.nextLine();
        String[] sortWords = allWords.toLowerCase().replaceAll("[-.?!)(,:]", "").split("\\s");

        Map<String, Integer> singleWords = new HashMap<>();
        for (String word : sortWords) {
            if (!word.isEmpty()) {
                Integer count = singleWords.get(word);
                if (count == null) {
                    count = 0;
                }
                singleWords.put(word, ++count);
            }
        }

        for (String word : singleWords.keySet()) {
            System.out.println("Count("+singleWords.get(word)+") " + word);
        }
    }
}