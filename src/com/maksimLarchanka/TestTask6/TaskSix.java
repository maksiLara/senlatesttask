package com.maksimLarchanka.TestTask6;

import java.util.Scanner;

public class TaskSix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter side A,B,C : ");

        Triangle triangle = new Triangle(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        if (triangle.isRectangular()) {
            System.out.println("Triangle is a rectangular");
        } else System.out.println("Triangle is not a rectangular");

    }
}