package com.maksimLarchanka.TestTask6;

public class Triangle {

    double sideA;
    double sideB;
    double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = Math.pow(sideA, 2);
        this.sideB = Math.pow(sideB, 2);
        this.sideC = Math.pow(sideC, 2);
    }

    public boolean isRectangular() {
        if (sideC == (sideA + sideB)) {
            return true;
        } else if (sideB == (sideA + sideC)) {
            return true;
        } else if (sideA == (sideB + sideC)) {
            return true;
        }
        return false;
    }


}


