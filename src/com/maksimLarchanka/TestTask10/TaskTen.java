package com.maksimLarchanka.TestTask10;

import java.util.Arrays;

import java.util.Scanner;

public class TaskTen {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] mass = new int[3];

        for (int i = 0; i < mass.length; i++) {
            mass[i] = scanner.nextInt();
        }
        combination(mass);
    }

    static void combination(int[] mass) {
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass.length - 1; j++) {
                System.out.println("Combination " + Arrays.toString(mass));
                int num = mass[j];
                mass[j] = mass[j + 1];
                mass[j + 1] = num;
            }
        }
    }
}