package com.maksimLarchanka.TestTask3;

import java.util.Scanner;

public class TaskThree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        StringBuilder stringBuilder = new StringBuilder(word);
        System.out.println(word.equals(stringBuilder.reverse().toString()));
    }
}