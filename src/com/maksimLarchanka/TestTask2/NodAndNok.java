package com.maksimLarchanka.TestTask2;

class NodAndNok {

    long getNod(long numberOne, long numberTwo) {
        if (numberTwo == 0)
            return numberOne;
        long value = numberOne % numberTwo;
        return getNod(numberTwo, value);
    }

    long getNok(long numberOne, long numberTwo) {
        long value = numberOne * numberTwo / getNod(numberOne, numberTwo);
        return value;
    }
}
