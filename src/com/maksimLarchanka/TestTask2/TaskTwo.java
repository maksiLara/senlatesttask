package com.maksimLarchanka.TestTask2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskTwo {

    public static void main(String[] args) {
        NodAndNok nodAndNok = new NodAndNok();

        Scanner scannerIn = new Scanner(System.in);
        System.out.println("Enter the number: ");
        try {
            long numOne = scannerIn.nextInt();
            long numTwo = scannerIn.nextInt();
            long valueNod = nodAndNok.getNod(numOne, numTwo);
            long valueNok = nodAndNok.getNok(numOne, numTwo);
            long sumNokAndNod = valueNod + valueNok;
            long resNokAndNod = valueNok - valueNod;
            System.out.println("Sum Nok and Nod = " + sumNokAndNod + " (Nok=" + valueNok + ", Nod=" + valueNod + ")");
            System.out.println("Residual Nok and Nod = " + resNokAndNod + " (Nok=" + valueNok + ", Nod=" + valueNod + ")");
        } catch (InputMismatchException e) {
            System.out.println("This number is fractional, please enter the integer number!");
        }

    }
}