package com.maksimLarchanka.TestTask8;

import java.util.Scanner;

public class TaskEight {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        String sNumber = String.valueOf(number);
        StringBuilder stringBuilder = new StringBuilder(sNumber);
        System.out.println(sNumber.equals(stringBuilder.append(number).reverse().toString()));
    }
}