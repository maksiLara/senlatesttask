package com.maksimLarchanka.TestTask7;

import java.util.Scanner;

public class TaskSeven {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] mass = new int[scanner.nextInt()];
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;

        for (int i = 0; i < mass.length; i++) {
            mass[i] = GeneretionValue.getRandom(10, 99);
            System.out.print(mass[i] + ", ");
        }
        for (int i = 0; i < mass.length; i++) {
            if (min > mass[i]) {
                min = mass[i];
            }
            if (max < mass[i]) {
                max = mass[i];
            }
        }
        System.out.println();
        System.out.println("MinValue = " + min);
        System.out.println("MaxValue = " + max);

    }

}