package com.maksimLarchanka.TestTask9;

import com.maksimLarchanka.TestTask7.GeneretionValue;

import java.util.Scanner;

public class TaskNine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sequence = scanner.nextInt();
        int sum = 0;

        for (int i = 0; i < sequence; i++) {
            int number = 0 + (int) (Math.random() * ((100 - 0) + 1));
            if ((number % 2) == 0) {
                System.out.println(number);
                sum = sum + number;
            }
        }
        System.out.println("Sum = " + sum);
    }
}