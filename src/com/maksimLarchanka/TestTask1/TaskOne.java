package com.maksimLarchanka.TestTask1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskOne {

    public static void main(String[] args) {
        Scanner scannerIn = new Scanner(System.in);
        System.out.println("Enter the number: ");
        try {
            int valueInt = scannerIn.nextInt();
            if (valueInt % 1 == 0) {
                System.out.println("This number is integer = " + valueInt);
            }
        } catch (InputMismatchException e) {
            System.out.println("This number is fractional, please enter the integer number!");
        }
    }
}
